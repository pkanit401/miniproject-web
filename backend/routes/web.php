<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Firebase\JWT\JWT;

date_default_timezone_set("Asia/Bangkok");
//Auth system
$router->post('/api/v1/login', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $password = $request->input("password");
    $result = app('db')->select(
        'SELECT account_ID,password,role from account WHERE Username = ?',
        [$username]
    );
    if ($result == null) {
        return response()->json(["STATUS" => "0"]);
    }
    $loginResult = new stdClass();
    if (count($result) == 0) {
        $loginResult->STATUS = "fail";
        $loginResult->reason = "User is not founded";
    } else {
        if (app('hash')->check($password, $result[0]->password)) {
            $loginResult->STATUS = "SUCCESS";
            $loginResult->role = $result[0]->role;
            $payload = [
                'iss' => "todolist",
                'sub' => $result[0]->account_ID,
                'iat' => time(),
                'exp' => time() + 30 * 60 * 60,
            ];
            $loginResult->token = JWT::encode($payload, env('APP_KEY'));
            return response()->json($loginResult);
        } else {
            $loginResult->STATUS = "fail";
            $loginResult->reason = "Incorrect Password";
            return response()->json($loginResult);
        }
    }
    return response()->json($loginResult);
});


$router->post('/api/v1/register', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $query = app('db')->select("SELECT username FROM account WHERE Username = ?", [$username]);
    if (count($query) > 0) {
        return response()->json(["STATUS" => "username is already taken"]);
    }
    $password = $request->input("password");
    $re_password = $request->input("re_password");
    $email = $request->input("email");
    $firstname = $request->input("firstname");
    $lastname = $request->input("lastname");
    $telephone = $request->input("telephone");
    if (strcmp($password, $re_password) == 0) {
        $password = app('hash')->make($password);
        $query = app('db')->insert(
            'INSERT INTO account(username,password,email,firstname,lastname,telephone) VALUES (?,?,?,?,?,?)',
            [$username, $password, $email, $firstname, $lastname, $telephone]
        );
        return response()->json(["STATUS" => "200"]);
    }
    return response()->json(["STATUS" => "100"]);
});

//booking system
$router->post('/api/v1/booking', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $machine = $request->input("machine_id");
    $machine_name = $request->input("machine_name");
    $account = app('db')->select("SELECT account_ID,firstname,lastname,telephone FROM account WHERE username = ? ", [$username]);
    if ($account == null) {
        return response()->json(["STATUS" => "FAIL"]);
    }
    $query = app('db')->insert(
        "INSERT INTO booking(account_ID,firstname,lastname,telephone,machine,booking_time) VALUES (?,?,?,?,?,?)",
        [$account[0]->account_ID, $account[0]->firstname, $account[0]->lastname, $account[0]->telephone, $machine_name, date("h:i:s")]
    );
    $query_notification = app('db')->insert("INSERT into notification (Details,account_ID,update_time) VALUES (?,?,?)", ["จองเครื่องเล่น " . $machine_name, $account[0]->account_ID, date("h:i:s")]);
    if ($query == null) {
        return response()->json(["STATUS" => "FAIL"]);
    } else if (isset($query)) {
        return response()->json(["STATUS" => "SUCCESS"]);
    }
}]);

$router->get('/api/v1/list_booking', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $list_booking = app('db')->select("SELECT * FROM booking");
    return response()->json($list_booking);
}]);

$router->put('/api/v1/confirm_booking', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $booking_ID  = $request->input("booking_ID");
    $account = app('db')->select("SELECT account_ID FROM account WHERE username = ? ", [$username]);
    if ($account == null)
        return response()->json(["STATUS" => "FAIL"]);

    $query_machine = app('db')->select("SELECT machine FROM booking WHERE booking_ID = ?", [$booking_ID]);
    $query_notification = app('db')->insert("INSERT into notification (Details,account_ID,update_time) VALUES (?,?,?)", ["ยืนยันการจองเครื่องเล่น " . $query_machine[0]->machine, $account[0]->account_ID, date("h:i:s")]);
    $result = app('db')->update("UPDATE booking SET payment_status = 1 WHERE account_ID = ? AND booking_ID = ? ", [$account[0]->account_ID, $booking_ID]);
    if ($result == null) {
        return response()->json(["STATUS" => "PAYMENT FAIL"]);
    } else if (isset($result)) {
        return response()->json(["Booking ID" => $booking_ID, "STATUS" => "SUCCESS"]);
    }
}]);

$router->get('/api/v1/confirm_booking_list', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $account = app('db')->select("SELECT account_ID FROM account WHERE username = ? ", [$username]);
    if ($account == null) {
        return response()->json(["STATUS" => "FAIL"]);
    }
    $result = app('db')->select("SELECT firstname,lastname,telephone,machine,payment_status FROM booking WHERE payment_status = 1");
    if ($result == null) {
        return response()->json(["STATUS" => "FAIL"]);
    } else if (isset($result)) {
        return response()->json($result);
    }
}]);

$router->get('/api/v1/unconfirm_booking_list', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $account = app('db')->select("SELECT account_ID FROM account WHERE username = ? ", [$username]);
    if ($account == null) {
        return response()->json(["STATUS" => "FAIL"]);
    }
    $result = app('db')->select("SELECT * FROM booking WHERE payment_status = 0 AND account_ID = ?", [$account[0]->account_ID]);

    if (count($result) == 0) {
        return response()->json(["STATUS" => "0"]);
    } else if (count($result) > 0) {
        return response()->json($result);
    }
}]);

$router->delete('/api/v1/delete_booking', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $booking_ID = $request->input("booking_ID");
    $account = app('db')->select("SELECT account_ID FROM account WHERE username = ? ", [$username]);
    if ($account == null) {
        return response()->json(["STATUS" => "FAIL"]);
    }
    $query_machine = app('db')->select("SELECT machine FROM booking WHERE booking_ID = ?", [$booking_ID]);
    $query_notification = app('db')->insert("INSERT into notification (Details,account_ID,update_time) VALUES (?,?,?)", ["ยกเลิกการจอง " . $query_machine[0]->machine, $account[0]->account_ID, date("h:i:s")]);
    $result = app('db')->delete("DELETE FROM booking WHERE account_ID = ? AND booking_ID = ? AND payment_status = 0", [$account[0]->account_ID, $booking_ID]);
    if ($result == null) {
        return response()->json(["STATUS" => "FAIL"]);
    } else if (isset($result)) {
        return response()->json(["STATUS" => "DELETED"]);
    }
}]);

//Account
$router->get('/api/v1/account', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $account = app('db')->select("SELECT * FROM account WHERE username = ?", [$username]);
    if ($account[0]->role == 0) {
        if ($account == null) {
            return response()->json(["STATUS" => "null"]);
        } else if (isset($account)) {
            return response()->json($account);
        }
    } else if ($account[0]->role == 1) {
        $query = app('db')->select("SELECT * FROM account");
        return response()->json($query);
    }
}]);

//Notification
$router->get('/api/v1/notification', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $account = app('db')->select("SELECT account_ID FROM account WHERE username = ? ", [$username]);
    if ($account == null) {
        return response()->json(["STATUS" => "FAIL"]);
    }
    $result = app('db')->select("SELECT Details,update_time FROM notification WHERE account_ID = ?", [$account[0]->account_ID]);
    if ($result == null) {
        return response()->json(["STATUS" => "null"]);
    } else if (isset($result)) {
        return response()->json($result);
    }
}]);

$router->delete('/api/v1/clear_notification', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $account = app('db')->select("SELECT account_ID FROM account WHERE username = ? ", [$username]);
    if ($account == null) {
        return response()->json(["STATUS" => "FAIL"]);
    }
    $clear = app('db')->delete("DELETE FROM notification WHERE account_ID = ?", [$account[0]->account_ID]);
}]);
//change password api
$router->put('/api/v1/changepassword', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $password = $request->input("password");
    $re_password = app('hash')->make($request->input("re_password"));
    $result = app('db')->select("SELECT password FROM account WHERE username = ?", [$username]);
    if (app('hash')->check($password, $result[0]->password)) {
        $change_pass = app('db')->update("UPDATE account SET password = ? WHERE username = ?", [$re_password, $username]);
        if ($change_pass == null) {
            return response()->json(["STATUS" => "FAIL"]);
        } else {
            return response()->json(["STATUS" => "SUCCESS"]);
        }
    } else {
        return response()->json(["STATUS" => "FAIL"]);
    }
}]);

// admin ZONE
$router->get('/api/v1/admin_page',  ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $query = app('db')->select("SELECT * FROM account");
    return response()->json(["ACCOUNT" => $query]);
}]);

$router->delete('/api/v1/admin_page',  ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $booking_id = $request->input("booking_id");
    $query = app('db')->delete("DELETE FROM booking WHERE booking_id = ?", [$booking_id]);
    if ($query)
        return response()->json(["STATUS" => "DELETED"]);
    else
        return response()->json(["STATUS" => "FAIL"]);
}]);

$router->delete('/api/v1/delete_user', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $account_id = $request->input("account_id");
    $delete_account = app('db')->delete("DELETE FROM account WHERE account_ID = ?", [$account_id]);
    $delete_notification = app('db')->delete("DELETE FROM notification WHERE account_ID = ?", [$account_id]);
    if ($delete_account && $delete_notification)
        return response()->json(["STATUS" => "DELETED"]);
    else
        return response()->json(["STATUS" => "FAILELD"]);
}]);


//total booking
$router->get('/api/v1/information', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $result = app('db')->select("SELECT * FROM account");
    $total_account = count($result);
    $result = app('db')->select("SELECT * FROM booking");
    $total_booking = count($result);
    $result = app('db')->select("SELECT * FROM item");
    $total_machine = count($result);

    $value = new stdClass();
    $value->account = $total_account;
    $value->booking = $total_booking;
    $value->machine = $total_machine;

    return response()->json($value);
}]);

$router->get('/api/v1/total_account', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $result = app('db')->select("SELECT * FROM account");
    return response()->json($result);
}]);
$router->get('/api/v1/total_machine', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $result = app('db')->select("SELECT * FROM item");
    return response()->json($result);
}]);
//update price 
$router->get('/api/v1/update_price',  ['middleeware' => 'auth', function (\Illuminate\Http\Request $request) {
    $item_id = $request->input("item_id");
    $new_price = $request->input("new_price");
    $query = app('db')->update("UPDATE item SET item_price = ? WHERE item_id = ?", [$new_price, $item_id]);
    if ($query)
        return response()->json(["STATUS" => "success"]);
    else
        return response()->json(["STATUS" => "there are somthing wrong!"]);
}]);

$router->delete('/api/v1/delete_account', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $account_ID = $request->input("account_ID");
    $result = app('db')->delete("DELETE FROM account WHERE account_ID = ?", [$account_ID]);
    if ($result)
        return response()->json(["STATUS" => "Deleted"]);
    else
        return response()->json(["STATUS" => "Fail"]);
}]);

//reset password user by admin
$router->put('/api/v1/update_password', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $account_ID = $request->input("account_ID");
    $password = $request->input("password");
    $password = app('hash')->make($password);
    $result = app('db')->update("UPDATE account SET password = ? WHERE account_ID = ?", [$password, $account_ID]);

    if ($result)
        return response()->json(["STATUS" => "UPDATED"]);
    else
        return response()->json(["STATUS" => "FAILED"]);
}]);


$router->post('/api/v1/upload_photo',  function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    if ($request->hasFile('photo')) {
        $this->validate($request, [
            'photo' => 'image|max:10000',
        ]);
        $path = './profile_photo';
        $filename  = $username . '.jpg';
        $request->file('photo')->move($path, $filename);
        app('db')->update("UPDATE account SET photo = ? WHERE username = ?", ['profile_photo/' . $username . '.jpg', $username]);
        return redirect('http://localhost:3000/profile');
    }
    return redirect('http://localhost:3000/profile');
});

$router->post('/api/v1/livechat', ['middleeware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $text = $request->input("text");
    $result = app('db')->insert("INSERT INTO livechat (msg,username) VALUES (?,?)", [$text, $username]);
    if ($username == "admin") {
        $delete = app('db')->delete("DELETE FROM livechat WHERE username = ?", [$username]);
    } else {
        return response()->json(["STATUS" => "SUCCESS"]);
    }
}]);

$router->get('/api/v1/chatmessage', function (\Illuminate\Http\Request $request) {
    $result = app('db')->select("SELECT * FROM livechat");
    if ($result == null) {
        return response()->json(["STATUS" => "FAIL"]);
    } else {
        return response()->json($result);
    }
});

$router->delete('/api/v1/deletemessage', function (\Illuminate\Http\Request $request) {
    $id = $request->input("id");
    return response()->json(["id" => $id]);
    $id = intval($id);
    $result = app('db')->delete("DELETE FROM livechat WHERE id = ?", [$id]);
    return redirect('http://localhost:3000/dashboard');
});


//machine
$router->get('/api/v1/machine', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $result = app('db')->select("SELECT * FROM item");
    return response()->json($result);
}]);

$router->get('/api/v1/notification', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $account = app('db')->select("SELECT account_ID FROM account WHERE username = ?", [$username]);
    $notification = app('db')->select("SELECT Details,update_time FROM notification WHERE account_ID = ?", [$account[0]->account_ID]);
    if ($notification)
        return response()->json($notification);
    else
        return response()->json(["STATUS" => "FAIL"]);
}]);

$router->delete('/api/v1/delete_machine', ['middleware' => 'auth', function (\Illuminate\Http\Request $request) {
    $item_id = $request->input("id");
    $delete = app('db')->delete("DELETE FROM item WHERE item_id = ?", [$item_id]);
    if ($delete)
        return response()->json(["STATUS" => "SUCCESS"]);
    return response()->json(["STATUS" => "FAILED"]);
}]);

$router->post('/api/v1/add_machine',  function (\Illuminate\Http\Request $request) {
    $machine_name = $request->input("machine_name");
    $price = $request->input("price");
    $price = (int)$price;
    if ($request->hasFile('photo')) {
        $this->validate($request, [
            'photo' => 'image|max:10000',
        ]);
        $path = './machine_photo';
        $filename  = $machine_name . '.jpg';
        $request->file('photo')->move($path, $filename);
        app('db')->insert("INSERT INTO item (item_name,item_price,photo) VALUES (?,?,?)", [$machine_name, $price, 'machine_photo/' . $machine_name . '.jpg']);
        return redirect('http://localhost:3000/add_machine');
    }
    return redirect('http://localhost:3000/add_machine');
});
