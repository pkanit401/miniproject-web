const rides = [
    {
        id: 1,
        title: "Santa Cruz",
        price: "200",
        thumbnailUrl: "/img/Santa_Cruz.jpg"
    },
    {
        id: 2,
        title: "Viking",
        price: "300",
        thumbnailUrl: "/img/Viking.jpg"
    },
    {
        id: 3,
        title: "Spinning",
        price: "250",
        thumbnailUrl: "/img/Spinning.jpg"
    },
    {
        id: 4,
        title: "Roller Coaster",
        price: "200",
        thumbnailUrl: "/img/Roller_Coaster.jpg"
    }
]

export default rides;
  