import "./App.css";
import Main from "./components/Main";
import Unconfirmlist from "./components/Unconfirmlist";
import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Signup from "./components/Signup";
import { useState, useEffect } from "react";
import Appbar from "./components/Appbar";
import AppbarAdmin from "./components/AppbarAdmin";
import LoginPage from "./components/LoginPage";
import Account from "./components/Account";
import Resetpassword from "./components/Resetpassword";
import Dashboard from "./components/Dashboard";
import Adminpage from "./components/Adminpage";
import Totalbooking from "./components/Totalbooking";
import TotalAccount from "./components/TotalAccount";
import TotalMachine from "./components/TotalMachine";
import Notification from "./components/Notification";
import AddMachine from "./components/AddMachine";
function App() {
  const [user, setUser] = useState(false);
  const [admin, setAdmin] = useState(false);
  useEffect(() => {
    let token = sessionStorage.getItem("token");
    let role = sessionStorage.getItem("role");
    if (token && role == 1) setAdmin(true);
    else if (token && role == 0) setUser(true);
  }, []);
  return (
    <div className="App">
      {user && (
        <Router>
          <div>
            <Appbar state={setUser} />
            <Switch>
              <Route exact path="/">
                <Main state={user} />
              </Route>
              <Route path="/unconfirm_list">
                <Unconfirmlist />
              </Route>
              <Route path="/profile">
                <Account />
              </Route>
              <Route path="/dashboard">
                <Dashboard />
              </Route>
              <Route path="/notification">
                <Notification />
              </Route>
            </Switch>
          </div>
        </Router>
      )}
      {admin && (
        <Router>
          <AppbarAdmin state={setAdmin} />
          <Switch>
            <Route exact path="/">
              <Adminpage />
            </Route>
            <Route path="/total_booking">
              <Totalbooking />
            </Route>
            <Route path="/total_account">
              <TotalAccount />
            </Route>
            <Route path="/total_machine">
              <TotalMachine />
            </Route>
            <Route path="/add_machine">
              <AddMachine />
            </Route>
          </Switch>
        </Router>
      )}
      {!user && !admin && (
        <Router>
          <Switch>
            <Route exact path="/">
              <LoginPage state={setUser} admin={setAdmin} />
            </Route>
            <Route path="/signup">
              <Signup />
            </Route>
            <Route path="/resetpassword">
              <Resetpassword />
            </Route>
          </Switch>
        </Router>
      )}
    </div>
  );
}

export default App;
