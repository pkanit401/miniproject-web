import axios from "axios";
import { useState } from "react";
import Swal from "sweetalert2";
import { BrowserRouter as Link, Route, Redirect } from "react-router-dom";

function Signup() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [re_password, setRePassword] = useState("");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [telephone, setTelephone] = useState("");
  function handleUsername(event) {
    let key = event.target.value;
    let n = key.length - 1;
    if (
      (key.charCodeAt(n) >= 48 && key.charCodeAt(n) <= 57) ||
      (key.charCodeAt(n) >= 65 && key.charCodeAt(n) <= 90) ||
      (key.charCodeAt(n) >= 97 && key.charCodeAt(n) <= 122)
    ) {
      setUsername(event.target.value);
    } else if (key !== "") {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Can not put " + key.charAt(n) + " to your username",
      });
      event.target.value = username;
    }
  }
  function handlePassword(event) {
    setPassword(event.target.value);
  }
  function handleRePassword(event) {
    setRePassword(event.target.value);
  }
  function handleFirstname(event) {
    setFirstname(event.target.value);
  }
  function handleLastname(event) {
    setLastname(event.target.value);
  }
  function handleEmail(event) {
    setEmail(event.target.value);
  }
  function handleTelephone(event) {
    let key = event.target.value;
    let n = key.length - 1;
    if (key.charCodeAt(n) >= 48 && key.charCodeAt(n) <= 57) {
      setTelephone(event.target.value);
    } else {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Can not input " + key + " into your telephonen number!",
      });
      event.target.value = telephone;
    }
  }
  function signup() {
    if (username.length < 6) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Username must be contain at least 6 letters",
      });
    } else if (password.length < 8) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Password must be contain at least 8 letters.",
      });
    } else if (password.localeCompare(re_password) !== 0) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Password is not the same",
      });
    } else if (firstname === "") {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please input your firstname",
      });
    } else if (lastname === "") {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please input your lastname",
      });
    } else if (telephone.length < 10) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Your phone number is incorrect",
      });
    } else {
      axios
        .post("http://localhost:8000/api/v1/register", {
          username: username,
          password: password,
          re_password: re_password,
          email: email,
          firstname: firstname,
          lastname: lastname,
          telephone: telephone,
        })
        .then((res) => {
          let status = res.data.STATUS;
          if (status === "200") {
            Swal.fire({
                icon: "success",
                title:"REGISTER",
                text:"sucess"
            })
            setUsername("");
            setPassword("");
            setRePassword("");
            setFirstname("");
            setLastname("");
            setEmail("");
            setTelephone("");
          } else if (status === "username is already taken") {
            alert("username is already taken");
          }
        });
    }
  }
  return (
    <div className="sign-up">
      <div className="wrapper fadeInDown">
        <div id="formContent">
          <a href="/" className="inactive underlineHover">
            Sign In{" "}
          </a>
          <h2 className="active">Sign Up</h2>
          <div className="fadeIn first">
            <img
              src="https://i.pinimg.com/736x/3f/94/70/3f9470b34a8e3f526dbdb022f9f19cf7.jpg"
              id="icon"
              alt="User Icon"
            />
          </div>
          <form>
            <input
              type="text"
              id="login"
              className="fadeIn second"
              value={username}
              placeholder="Username"
              onChange={(e) => handleUsername(e)}
            />
            <input
              type="password"
              id="password"
              className="fadeIn third"
              value={password}
              placeholder="Password"
              onChange={(e) => handlePassword(e)}
            />
            <input
              type="password"
              id="re-password"
              className="fadeIn third"
              value={re_password}
              placeholder="re-password"
              onChange={(e) => handleRePassword(e)}
            />
            <input
              type="text"
              id="firstname"
              className="fadeIn third"
              value={firstname}
              placeholder="Firstname"
              onChange={(e) => handleFirstname(e)}
            />
            <input
              type="text"
              id="lastname"
              className="fadeIn third"
              value={lastname}
              placeholder="Lastname"
              onChange={(e) => handleLastname(e)}
            />
            <input
              type="email"
              id="email"
              className="fadeIn third"
              value={email}
              placeholder="Email"
              onChange={(e) => handleEmail(e)}
            />
            <input
              type="text"
              id="telephone"
              className="fadeIn third"
              value={telephone}
              placeholder="Telephone"
              onChange={(e) => handleTelephone(e)}
            />
            <input
              type="button"
              className="fadeIn fourth"
              value="Register"
              onClick={() => signup()}
            />
          </form>
        </div>
      </div>
    </div>
  );
}
export default Signup;
