import axios from 'axios';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
function Test(props) {
    const [status, setStatus] = useState(false);
    const [all, setAll] = useState([]);
    useEffect(() => {
        axios.get("http://localhost:8000/api/v1/unconfirm_booking_list?api_token=" + sessionStorage.getItem('token') + "&username=" + sessionStorage.getItem('username') + "").then(res => {
            if (res.data.STATUS !== "0") {
                for (let i = 0; i < res.data.length; i++) {
                    console.log(res.data[i].booking_ID);
                    all.push({
                        id: res.data[i].booking_ID,
                        firstname: res.data[i].firstname,
                        lastname: res.data[i].lastname,
                        telephone: res.data[i].telephone,
                        machine: res.data[i].machine,
                        payment: "ยังไม่ได้ชำระ"
                    })
                }
                setStatus(true);
            }
        })
    }, [])
    function cancel(id) {
        let booking_id = id;
        axios.delete("http://localhost:8000/api/v1/delete_booking?api_token=" + sessionStorage.getItem("token") + "&username=" + sessionStorage.getItem("username") + "&booking_ID=" + booking_id).then(async (res) => {
            if (res.data.STATUS === "DELETED") {
                await Swal.fire({
                    icon: 'success',
                    title: 'SUCCESS...',
                    text: 'Booking success!'
                })
                window.location.reload();

            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'FAILED...',
                    text: 'DELETE FAILED'
                })
            }
        })

    }
    function confirmBooking(id) {
        let booking_ID = id;
        axios.put("http://localhost:8000/api/v1/confirm_booking", {
            "api_token": sessionStorage.getItem("token"),
            "username": sessionStorage.getItem("username"),
            "booking_ID": booking_ID
        }).then(async (res) => {
            if (res.data.STATUS === "SUCCESS") {
                await Swal.fire({
                    icon: 'success',
                    title: 'SUCCESS...',
                    text: 'Confirm success!'
                })
                window.location.reload();
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'FAILED...',
                    text: 'Confirm FAILED'
                })
            }
        })
    }
    return (
        <div>
            {status ?
                (
                    <div>
                        <h1>Unconfirm payment</h1>
                        {
                            all.map(item => {
                                return <div className="list-item">
                                    <li>Machine : {item.machine}<br />Payment : {item.payment}</li>
                                    <button className="confirm_btn" onClick={() => { confirmBooking(item.id) }}>ยืนยัน</button>
                                    <button className="unconfirm_btn" onClick={() => { cancel(item.id) }}>ยกเลิกจอง</button>
                                </div>
                            })
                        }
                    </div>
                ) :
                (
                    <div><h1>NO unconfirm</h1></div>
                )
            }

        </div>
    );
}

export default Test;