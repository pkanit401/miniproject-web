import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import down_arrow from "../icon/down-arrow.png";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect,
} from "react-router-dom";

function Appbar(props) {
  const [img, setImg] = useState("");
  const [firstname, setFirstname] = useState("");
  function logout() {
    sessionStorage.clear();
    props.state(false);
    window.location.href = "/";
  }
  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
    },
    paper: {
      marginRight: theme.spacing(2),
    },
  }));

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }
    prevOpen.current = open;
  }, [open]);
  useEffect(async () => {
    axios
      .get(
        "http://localhost:8000/api/v1/account?api_token=" +
          sessionStorage.getItem("token") +
          "&username=" +
          sessionStorage.getItem("username") +
          ""
      )
      .then((res) => {
        setImg("http://localhost:8000/" + res.data[0].photo);
        setFirstname(res.data[0].firstname);
      });
  }, []);
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <div className="bar">
            <Typography variant="h6" className={classes.title}>
              <Button>
                <a href="/">
                  <img
                    src="https://cdn.icon-icons.com/icons2/2248/PNG/512/home_circle_icon_137496.png"
                    alt="Italian truly"
                    style={{ width: "50px", left: "0%" }}
                  />
                </a>
              </Button>
            </Typography>
          </div>
          <div className={classes.root}>
            <a href="/profile">
              <img className="Appbar-Photo" src={img} alt="Avatar" />
            </a>
            <div>
              <Button
                ref={anchorRef}
                aria-controls={open ? "menu-list-grow" : undefined}
                aria-haspopup="true"
                onClick={handleToggle}
                style={{
                  position: "absolute",
                  margin: "auto",
                  right: "-0%",
                  width: "100px",
                  marginTop: "-20px",
                }}
              >
                <img src={down_arrow} style={{ width: "30px" }} />
              </Button>
              <Popper
                open={open}
                anchorEl={anchorRef.current}
                role={undefined}
                transition
                disablePortal
              >
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "center bottom",
                    }}
                  >
                    <Paper>
                      <ClickAwayListener onClickAway={handleClose}>
                        <MenuList
                          autoFocusItem={open}
                          id="menu-list-grow"
                          onKeyDown={handleListKeyDown}
                        >
                          <MenuItem onClick={handleClose}>
                            <Link to="/profile">Profile</Link>
                          </MenuItem>
                          <MenuItem>
                            <Link to="/notification">Notification</Link>
                          </MenuItem>
                          <MenuItem>
                            <Link to="/unconfirm_list">Unconfirm List</Link>
                          </MenuItem>
                          <MenuItem onClick={logout}>Logout</MenuItem>
                        </MenuList>
                      </ClickAwayListener>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            </div>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default Appbar;
