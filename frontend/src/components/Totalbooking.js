import axios from "axios";
import { useEffect, useState } from "react";
import "./Totalbooking.css";
function Totalbooking() {
  const [list, setList] = useState([]);
  const [status, setStatus] = useState(false);
  useEffect(() => {
    axios
      .get(
        "http://localhost:8000/api/v1/list_booking?api_token=" +
          sessionStorage.getItem("token")
      )
      .then((res) => {
        for (let i = 0; i < res.data.length; i++) {
          let bID = res.data[i].booking_ID;
          let aID = res.data[i].account_ID;
          let firstname = res.data[i].firstname;
          let lastname = res.data[i].lastname;
          let telephone = res.data[i].telephone;
          let machine = res.data[i].machine;
          let payment =
            res.data[i].payment_status === 1 ? "ชำระเสร็จสิ้น" : "ยังไม่ชำระ";
          let time = res.data[i].booking_time;
          list.push({
            booking_id: bID,
            account: aID,
            firstname: firstname,
            lastname: lastname,
            telephone: telephone,
            machine: machine,
            payment: payment,
            time: time,
          });
        }
        setStatus(true);
      });
  },[]);
  return (
    <div>
      {status && (
        <table border={1} >
          <tr>
            <th>Booking ID</th>
            <th>account ID</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Telephone</th>
            <th>machine</th>
            <th>payment</th>
            <th>time</th>
          </tr>
          {list.map((item) => {
            return (
              <tr>
                <td>{item.booking_id}</td>
                <td>{item.account}</td>
                <td>{item.firstname}</td>
                <td>{item.lastname}</td>
                <td>{item.telephone}</td>
                <td>{item.machine}</td>
                <td>{item.payment}</td>
                <td>{item.time}</td>
              </tr>
            );
          })}
        </table>
      )}
    </div>
  );
}
export default Totalbooking;
