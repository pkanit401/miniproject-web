import "./style.css";
import { useState, useEffect } from "react";
import axios from "axios";
import Unconfirmlist from "./Unconfirmlist";
import { BrowserRouter as Link } from "react-router-dom";
import Swal from "sweetalert2";

function LoginPage(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState("");
  const [status, setStatus] = useState(false);
  useEffect(() => {
    if (sessionStorage.getItem("status")) {
      axios
        .post("http://localhost:8000/api/v1/login", {
          username: sessionStorage.getItem("username"),
          password: sessionStorage.getItem("password"),
        })
        .then((res) => {
          props.state(true);
          props.setAdmin(true);
        });
    }
  }, []);

  function login() {
    let user = username;
    let pass = password;
    axios
      .post("http://localhost:8000/api/v1/login", {
        username: user,
        password: pass,
      })
      .then((res) => {
        let role = res.data.role;
        let status = res.data.STATUS;
        let token = res.data.token;
        if (status === "SUCCESS" && token !== null) {
          sessionStorage.setItem("username", user);
          sessionStorage.setItem("token", token);
          sessionStorage.setItem("role", role);
          setStatus(true);
          setToken(token);
          if (role === 1) props.admin(true);
          else if (role === 0) props.state(true);
        } else if (status === "fail" || status === "0") {
          Swal.fire({
            icon: "error",
            title: "There is something went wrong!",
            text: "Your username or password is incorrect!",
          });
        }
      });
  }

  function handleUsername(event) {
    setUsername(event.target.value);
  }
  function handlePassword(event) {
    setPassword(event.target.value);
  }
  function handleKey(event) {
    if (event.key === "Enter") {
      login();
    }
  }
  return (
    <div className="LoginPage" onKeyDown={handleKey}>
      <div className="wrapper fadeInDown">
        <div id="formContent">
          <h2 className="active"> Sign In </h2>
          <a href="/signup" className="underlineHover">
            Sign UP
          </a>
          <div className="fadeIn first">
            <img
              src="https://i.pinimg.com/736x/3f/94/70/3f9470b34a8e3f526dbdb022f9f19cf7.jpg"
              id="icon"
              alt="User Icon"
            />
          </div>
          <form>
            <input
              type="text"
              id="login"
              className="fadeIn second"
              placeholder="Username"
              onChange={(e) => handleUsername(e)}
            />
            <input
              type="password"
              id="password"
              className="fadeIn third"
              placeholder="Password"
              onChange={(e) => handlePassword(e)}
            />
            <input
              type="button"
              className="fadeIn fourth"
              value="Log In"
              onClick={() => login()}
            />
          </form>
        </div>
      </div>
    </div>
  );
}
export default LoginPage;
