import { useEffect, useState } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';
import './TotalAccount.css'
function TotalAccount() {
    const [account, setAccount] = useState([]);
    const [status, setStatus] = useState(false);
    useEffect(() => {
        axios.get('http://localhost:8000/api/v1/total_account?api_token=' + sessionStorage.getItem("token")).then(res => {
            for (let i = 0; i < res.data.length; i++) {
                account.push({
                    account_id: res.data[i].account_ID,
                    username: res.data[i].username,
                    firstname: res.data[i].firstname,
                    lastname: res.data[i].lastname,
                    telephone: res.data[i].telephone,
                    photo: "http://localhost:8000/" + res.data[i].photo
                })
            }
            setStatus(true);
        })
    })
    function deleteAccount(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "DELETE ACCOUNT!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, confirm!",
        }).then((result) => {
            if (result.isConfirmed) {
                axios.delete('http://localhost:8000/api/v1/delete_user?api_token=' + sessionStorage.getItem("token") + "&account_id=" + id).then(res => {
                    if (res.data.STATUS === "DELETED") {
                        Swal.fire({
                            icon: "success",
                            title: "SUCCESS...",
                            text: "DELETE success!",
                        }).then(() => {
                            window.location.reload();
                        });
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "FAILED...",
                            text: "DELETE failed!",
                        });
                    }
                })
            }
        });
    }
    return (
        <div>
            {status &&
                <div>
                    <table border={1} className="table-account">
                        <thead>
                            <tr>
                                <th>account_id</th>
                                <th>username</th>
                                <th>firstname</th>
                                <th>lastname</th>
                                <th>telephone</th>
                                <th>photo</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                account.map(item => {
                                    return <tr>
                                        <td>{item.account_id}</td>
                                        <td>{item.username}</td>
                                        <td>{item.firstname}</td>
                                        <td>{item.lastname}</td>
                                        <td>{item.telephone}</td>
                                        <td><img src={item.photo} alt="Avatar" style={{ width: "50px" }} /></td>
                                        <td><button onClick={() => { deleteAccount(item.account_id) }}>delete {item.username}</button></td>
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>
                </div>
            }
        </div>
    );
}
export default TotalAccount;