import React, { useState } from "react";
import { Link } from "react-router-dom";
//import react pro sidebar components
import Big from "../icon/big.png";
import Samll from "../icon/small.png";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from "react-pro-sidebar";

//import icons from react icons
import { FaList, FaRegHeart } from "react-icons/fa";
import {
  FiHome,
  FiLogOut,
  FiArrowLeftCircle,
  FiArrowRightCircle,
} from "react-icons/fi";
import { ImPlus } from "react-icons/im";

//import sidebar css from react-pro-sidebar module and our custom css
import "react-pro-sidebar/dist/css/styles.css";
import "./Header.css";

function AppbarAdmin(props) {
  const [menuCollapse, setMenuCollapse] = useState(false);

  //create a custom function that will change menucollapse state from false to true and true to false
  const menuIconClick = () => {
    //condition checking to change state from true to false and vice versa
    menuCollapse ? setMenuCollapse(false) : setMenuCollapse(true);
  };
  function logout() {
    sessionStorage.clear();
    props.state(false);
  }
  function redi(temp) {
    if (temp === 1) window.location.href = "/";
    else if (temp === 2) window.location.href = "/add_machine";
  }
  return (
    <>
      <div id="header">
        {/* collapsed props to change menu size using menucollapse state */}
        <ProSidebar collapsed={menuCollapse}>
          <SidebarHeader>
            <div className="logotext">
              {/* small and big change using menucollapse state */}
              <p>
                {menuCollapse ? (
                  <img src={Big} style={{ width: "30px" }} />
                ) : (
                  <img src={Big} style={{ width: "50px" }} />
                )}
              </p>
            </div>
            <div className="closemenu" onClick={menuIconClick}>
              {/* changing menu collapse icon on click */}
              {menuCollapse ? <FiArrowRightCircle /> : <FiArrowLeftCircle />}
            </div>
          </SidebarHeader>
          <SidebarContent>
            <Menu iconShape="square">
              <MenuItem
                active={true}
                icon={<FiHome />}
                onClick={() => {
                  redi(1);
                }}
              >
                Home
              </MenuItem>
              <MenuItem
                icon={<ImPlus />}
                onClick={() => {
                  redi(2);
                }}
              >
                Add Machine
              </MenuItem>
            </Menu>
          </SidebarContent>
          <SidebarFooter>
            <Menu iconShape="square">
              <MenuItem
                icon={<FiLogOut />}
                onClick={() => {
                  logout();
                }}
              >
                <Link to="/">Logout</Link>
              </MenuItem>
            </Menu>
          </SidebarFooter>
        </ProSidebar>
      </div>
    </>
  );
}
export default AppbarAdmin;
