import axios from "axios";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";

function Notification() {
    const [notification, setNotification] = useState([]);
    const [status, setStatus] = useState(false)
    useEffect(() => {
        axios.get("http://localhost:8000/api/v1/notification?api_token=" + sessionStorage.getItem("token") + "&username=" + sessionStorage.getItem("username")).then(res => {
            for (let i = 0; i < res.data.length; i++) {
                notification.push({
                    "details": res.data[i].Details,
                    "time": res.data[i].update_time
                })
            }
            console.log(res.data);
            setStatus(true);
        })
    }, [])
    function clearNotification() {
        axios.delete("http://localhost:8000/api/v1/clear_notification?api_token=" + sessionStorage.getItem("token") + "&username=" + sessionStorage.getItem("username")).then(() => {
            window.location.reload();
        })
    }
    return (
        <div>
            {
                status ?
                    (
                        <div>
                            <h1>Notification</h1><button onClick={() => clearNotification()}>ล้างการแจ้งเตือน</button>
                            {notification.map(item => {
                                return <div className="notification">
                                    <div className="notification_detail">
                                        {item.details}<br />
                                        เวลา {item.time}
                                    </div>
                                </div>
                            })}
                        </div>
                    ) : (
                        <div>
                            No notification
                        </div>)
            }
        </div >
    );
}
export default Notification;