import axios from "axios";
import { useState, useEffect } from "react";
import "./style.css";
import Swal from "sweetalert2";
function Main(props) {
  const [machine, setMachine] = useState([]);
  const [status, setStatus] = useState(false);
  useEffect(async () => {
    await axios
      .get(
        "http://localhost:8000/api/v1/machine?api_token=" +
          sessionStorage.getItem("token")
      )
      .then((res) => {
        for (let i = 0; i < res.data.length; i++) {
          machine.push({
            id: res.data[i].item_id,
            name: res.data[i].item_name,
            price: res.data[i].item_price,
            photo: "http://localhost:8000/" + res.data[i].photo,
          });
        }
        setStatus(true);
      });
  }, []);

  function selectMachine(id, name) {
    let machine_id = id;
    let machine_name = name;
    Swal.fire({
      title: "Are you sure?",
      text: "Booking system!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, confirm!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post("http://localhost:8000/api/v1/booking?", {
            api_token: sessionStorage.getItem("token"),
            username: sessionStorage.getItem("username"),
            machine_id: machine_id,
            machine_name: machine_name,
          })
          .then((res) => {
            if (res.data.STATUS === "SUCCESS") {
              Swal.fire({
                icon: "success",
                title: "SUCCESS...",
                text: "Booking success!",
              });
            }
          });
      }
    });
  }

  return (
    <div>
      {status && (
        <div>
          <div className="machine_card">
            {machine.map((item) => {
              return (
                <div className="card">
                  <img
                    src={item.photo}
                    alt="Italian Truli"
                    className="machine_photo"
                  />
                  <div className="container">
                    <h2>MACHINE NAME : {item.name}</h2>
                    <br />
                    <p>PRICE : {item.price} BAHT</p>
                    <br />
                    <button
                      onClick={() => {
                        selectMachine(item.id, item.name);
                      }}
                    >
                      เลือก {item.name}
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </div>
  );
}
export default Main;
