import { useState } from 'react';
import './AddMachine.css';
function AddMachine() {
    const [img, setImg] = useState("");
    const [username, setUsername] = useState("");

    function handleImage(event) {
        let imge = URL.createObjectURL(event.target.files[0]);
        setImg(imge);
    }
    return (
        <div>
            <img src={img} style={{ width: "300px" }, { height: "500px" }} />
            <form
                action="http://localhost:8000/api/v1/add_machine"
                method="post"
                encType="multipart/form-data"
            >
                <input
                    type="file"
                    id="img"
                    name="photo"
                    accept="image/*"
                    onChange={(e) => {
                        handleImage(e);
                    }}
                />
                <br />
                <label htmlFor="machine_name">ชื่อเครื่องเล่น</label><br />
                <input
                    type="text"
                    name="machine_name"
                    style={{ width: "200px" }}

                />
                <br />
                <label htmlFor="machine_name">ราคา</label><br />
                <input
                    type="text"
                    name="price"
                    style={{ width: "200px" }}
                />
                <br />
                <input type="submit" value="Upload" />
            </form>
        </div>
    );
}

export default AddMachine;