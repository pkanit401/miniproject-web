import { useState, useEffect } from 'react';
import axios from 'axios';

function Dashboard() {
    const [text, setText] = useState("");
    const [dashboard, setDashboard] = useState([]);
    const [status, setStatus] = useState(false);
    const [temp, setTemp] = useState(0);
    useEffect(async () => {
        await axios.get('http://localhost:8000/api/v1/chatmessage?username=kanit').then(res => {
            for (let i = 0; i < res.data.length; i++) {
                dashboard.push({
                    id: res.data[i].id,
                    username: res.data[i].username,
                    text: res.data[i].msg
                });
            }
        })
        setStatus(true);
    }, 100000);

    function handleText(event) {
        setText(event.target.value);
    }

    function handleClick() {
        if (text != "") {
            axios.post('http://localhost:8000/api/v1/livechat?api_token=' + sessionStorage.getItem("token") + '', {
                username: sessionStorage.getItem("username"),
                text: text
            }).then(res => {
                console.log(res.data);
                document.location.reload();
            })
        }
    }
    function pressEnter(event) {
        if (event.key === "Enter")
            handleClick();
    }
    function handleEditCLick(event) {
        let id = event
        axios.delete('http://localhost:8000/api/v1/deletemessage', {
            id: id
        }).then(res => {
            console.log(res.data)
        })
    }
    return (
        <div onKeyDown={(e) => { pressEnter(e) }}>
            <div className="dashboard-field">
                <table>
                    <thead>
                        <th>Name</th>
                        <th>Text</th>
                    </thead>
                    {dashboard.map(item => {
                        return <tbody>
                            <td>
                                {item.username}
                            </td>
                            <td style={{ textAlign: "left" }}>
                                {item.text}
                                {sessionStorage.getItem("username") == "admin123" && <a style={{ color: "blue", float: "right", textAlign: "right" }} onClick={() => { handleEditCLick(item.id) }}>Edit</a>}
                            </td>
                        </tbody>
                    })}
                </table>
            </div>

            <div className="dashboard_input">
                <input type="text" placeholder="input text" onChange={(e) => { handleText(e) }} />
            </div>
            <input type="button" className="fadeIn fourth" onClick={() => handleClick()} value="Send" />
        </div>
    );
}
export default Dashboard;