import { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import "./Adminpage.css";
function Adminpage() {
    const [totalBooking, setTotalBooking] = useState(0);
    const [totalAccount, setTotalAccount] = useState(0);
    const [totalMachihne, setTotalMachine] = useState(0);
    useEffect(() => {
        axios.get('http://localhost:8000/api/v1/information?api_token=' + sessionStorage.getItem("token")).then((res) => {
            let account = res.data.account;
            let booking = res.data.booking;
            let item = res.data.machine;
            setTotalMachine(item);
            setTotalBooking(booking);
            setTotalAccount(account);
        });
    });
    function booking_btn() {
        window.location.href = "/total_booking";
    }
    function account_btn() {
        window.location.href = "/total_account";
    }
    function machine_btn() {
        window.location.href = "/total_machine";
    }
    return (
        <div>
            <div className="dashboard">
                <h1 style={{ textAlign: "left" }}>Dashboard</h1>
                <div className="card_total_booking">
                    <h1>{totalBooking}</h1>
                    <p>Total Booking</p>
                    <button id="booking_btn" onClick={() => { booking_btn() }}>More info</button>
                </div>
                <div className="card_total_account">
                    <h1>{totalAccount}</h1>
                    <p>Total Account</p>
                    <button id="account_btn" onClick={() => { account_btn() }}>More info</button>
                </div>
                <div className="card_total_machine">
                    <h1>{totalMachihne}</h1>
                    <p>Total Machine</p>
                    <button id="machine_btn" onClick={() => { machine_btn() }}>More info</button>
                </div>
            </div>
        </div>
    );
}
export default Adminpage;