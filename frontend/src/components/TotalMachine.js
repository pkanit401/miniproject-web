import React from 'react';
import { useEffect, useState } from "react";
import axios from "axios";
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table';
function TotalMachine() {
    const [machine, setMachine] = useState([]);
    const [status, setStatus] = useState(false);

    useEffect(() => {
        axios
            .get(
                "http://localhost:8000/api/v1/total_machine?api_token=" +
                sessionStorage.getItem("token")
            )
            .then((res) => {
                for (let i = 0; i < res.data.length; i++) {
                    machine.push({
                        machine_id: res.data[i].item_id,
                        machine_name: res.data[i].item_name,
                        machine_price: res.data[i].item_price,
                        photo: "http://localhost:8000/" + res.data[i].photo
                    })
                }
                setStatus(true);
            });
    }, []);
    function deleteMachine(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                axios.delete("http://localhost:8000/api/v1/delete_machine?api_token=" + sessionStorage.getItem("token") + "&id=" + id).then(res => {
                    let status = res.data.STATUS;
                    if (status === "SUCCESS")
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    window.location.reload();
                })
            }
        })
    }

    return (
        <div>
            {status &&
                <div>
                    Total Machine
                    <div className="machine_card" style={{ marginLeft: "10%" }}>
                        {machine.map(item => {
                            return <div className="card">
                                <img src={item.photo} alt="Italian Truli" className="machine_photo" />
                                <div className="container">
                                    <h2>ID : {item.machine_id}</h2>
                                    <h2>MACHINE NAME : {item.machine_name}</h2><br />
                                    <p>PRICE : {item.machine_price} BAHT</p><br />
                                    <button onClick={() => { deleteMachine(item.machine_id) }}>ลบ {item.machine_name}</button>
                                </div>
                            </div>
                        })}
                    </div>
                </div>
            }
        </div>
    );
}
export default TotalMachine;
