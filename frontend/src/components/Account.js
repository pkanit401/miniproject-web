import { useEffect, useState } from "react";
import React from "react";
import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";
import axios from "axios";
import { Link, Redirect } from "react-router-dom";
function Account() {
  const [status, setStatus] = useState(false);
  const [profile, setProfile] = useState([]);
  const [username, setUsername] = useState("");
  const [img, setImg] = useState("");
  useEffect(async () => {
    await axios
      .get(
        "http://localhost:8000/api/v1/account?api_token=" +
          sessionStorage.getItem("token") +
          "&username=" +
          sessionStorage.getItem("username")
      )
      .then((res) => {
        setImg("http://localhost:8000/" + res.data[0].photo);
        setUsername(res.data[0].username);
        for (let i = 0; i < res.data.length; i++) {
          profile.push({
            username: res.data[i].username,
            firstname: res.data[i].firstname,
            lastname: res.data[i].lastname,
            email: res.data[i].email,
            telephone: res.data[i].telephone,
          });
        }
        setStatus(true);
      });
  }, []);
  function handleImage(event) {
    let imge = URL.createObjectURL(event.target.files[0]);
    setImg(imge);
  }
  return (
    <div className="profile_page">
      {status && (
        <div>
          <div id="formContent">
            <h1>Profile</h1>
            {profile.map((item) => {
              return (
                <div>
                  <div>
                    <div>
                      <img className="photo" src={img} />
                    </div>
                    <Popup
                      trigger={
                        <button className="button"> Upload photo </button>
                      }
                      modal
                      nested
                    >
                      <div className="upload-form">
                        <h1>SELECT IMAGE</h1>
                        {img && (
                          <img className="photo" src={img} alt="Avatar" />
                        )}
                        <div>
                          <form
                            action="http://localhost:8000/api/v1/upload_photo"
                            method="post"
                            encType="multipart/form-data"
                          >
                            <input
                              type="file"
                              id="img"
                              name="photo"
                              accept="image/*"
                              onChange={(e) => {
                                handleImage(e);
                              }}
                            />
                            <input
                              type="hidden"
                              name="username"
                              value={username}
                            />
                            <br />
                            <input type="submit" value="Upload" />
                          </form>
                        </div>
                        <br />
                      </div>
                    </Popup>
                  </div>
                  <div>
                    <label htmlFor="username">Username</label>
                    <input type="text" value={item.username} disabled />
                  </div>
                  <div>
                    <label htmlFor="firstname">Firstname</label>
                    <input type="text" value={item.firstname} disabled />
                  </div>
                  <div>
                    <label htmlFor="lastname">Lastname</label>
                    <input type="text" value={item.lastname} disabled />
                  </div>
                  <div>
                    <label htmlFor="email">Email</label>
                    <br />
                    <input type="text" value={item.email} disabled />
                  </div>
                  <div>
                    <label htmlFor="telephone">Telephone</label>
                    <br />
                    <input type="text" value={item.telephone} disabled />
                  </div>
                  <a href="/">
                    <input type="button" value="OK" />
                  </a>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </div>
  );
}
export default Account;
