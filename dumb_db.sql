-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for play_park
CREATE DATABASE IF NOT EXISTS `play_park` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `play_park`;

-- Dumping structure for table play_park.account
CREATE TABLE IF NOT EXISTS `account` (
  `account_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `role` int(1) NOT NULL DEFAULT '0',
  `photo` varchar(250) DEFAULT 'profile_photo/default.png',
  PRIMARY KEY (`account_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- Dumping data for table play_park.account: ~2 rows (approximately)
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
REPLACE INTO `account` (`account_ID`, `username`, `password`, `firstname`, `lastname`, `email`, `telephone`, `role`, `photo`) VALUES
	(1, 'admin1234', '$2y$10$qwjbvblBdVEKJ6x0D4PqYeI/CCUXfENWeEN3mxK1vPmhWAsLzvEJW', 'admin1234', 'admin1234', 'admin1234@admin1234.com', '1234567890', 1, 'profile_photo/admin1234.jpg'),
	(54, 'kanit1', '$2y$10$mPJ7sZBMqZ0vTvWqTxIcbugKUj4z0fCX8Mt3YKWlLs9QfXEOEAR42', 'kanit1', 'kanit1', 'kanit1@kanit1.com', '1234567890', 0, 'profile_photo/default.png');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;

-- Dumping structure for table play_park.booking
CREATE TABLE IF NOT EXISTS `booking` (
  `booking_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_ID` int(10) unsigned NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `machine` varchar(50) NOT NULL,
  `payment_status` int(1) NOT NULL DEFAULT '0',
  `booking_time` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`booking_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- Dumping data for table play_park.booking: ~3 rows (approximately)
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
REPLACE INTO `booking` (`booking_ID`, `account_ID`, `firstname`, `lastname`, `telephone`, `machine`, `payment_status`, `booking_time`) VALUES
	(43, 53, 'kanit', 'kanit', '0980186275', 'Viking', 0, '08:32:46'),
	(44, 53, 'kanit', 'kanit', '0980186275', 'Giant drop', 0, '08:32:47'),
	(45, 54, 'kanit1', 'kanit1', '1234567890', 'Viking', 1, '09:12:10');
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;

-- Dumping structure for table play_park.item
CREATE TABLE IF NOT EXISTS `item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(50) DEFAULT NULL,
  `item_price` int(10) unsigned NOT NULL DEFAULT '0',
  `photo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table play_park.item: ~3 rows (approximately)
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
REPLACE INTO `item` (`item_id`, `item_name`, `item_price`, `photo`) VALUES
	(1, 'Viking', 300, 'machine_photo/viking.jpg'),
	(2, 'Giant drop', 500, 'machine_photo/giant drop.jpg'),
	(5, 'รถไฟเหาะ', 500, 'machine_photo/รถไฟเหาะ.jpg');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;

-- Dumping structure for table play_park.livechat
CREATE TABLE IF NOT EXISTS `livechat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `msg` varchar(10000) NOT NULL DEFAULT '',
  `username` varchar(50) NOT NULL DEFAULT '',
  `to` varchar(50) NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table play_park.livechat: ~0 rows (approximately)
/*!40000 ALTER TABLE `livechat` DISABLE KEYS */;
/*!40000 ALTER TABLE `livechat` ENABLE KEYS */;

-- Dumping structure for table play_park.notification
CREATE TABLE IF NOT EXISTS `notification` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Details` varchar(500) DEFAULT NULL,
  `account_ID` int(11) NOT NULL,
  `update_time` time DEFAULT '00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table play_park.notification: ~2 rows (approximately)
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
REPLACE INTO `notification` (`ID`, `Details`, `account_ID`, `update_time`) VALUES
	(1, 'จองเครื่องเล่น Viking', 54, '09:12:10'),
	(2, 'ยืนยันการจองเครื่องเล่น Viking', 54, '09:12:47');
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
